class CreateCharges < ActiveRecord::Migration[5.2]
  def change
    create_table :charges do |t|
      t.string :how_to_charge
      t.string :address
      t.string :phone1
      t.string :phone2
      t.text :description
      t.float :value
      t.datetime :charge_at
      t.boolean :paid, default: false

      t.timestamps
    end
  end
end
