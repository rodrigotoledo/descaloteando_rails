class ChangeChargeAtToDate < ActiveRecord::Migration[5.2]
  def change
    change_column :charges, :charge_at, :date
  end
end
