class FixChargesTable < ActiveRecord::Migration[5.2]
  def change
    rename_column :charges, :how_to_charge, :who_to_charge
    add_reference :charges, :user, foreign_key: true
  end
end
