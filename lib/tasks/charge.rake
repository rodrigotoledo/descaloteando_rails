require 'faker'
namespace :charge do
  desc "Reseting Charge with first user"
  task reset_data: :environment do
    Charge.destroy_all
    user = User.where(email: 'rodrigo@rtoledo.inf.br').first_or_create do |user|
      user.name = 'Rodrigo Toledo'
      user.phone = '33991221596'
      user.address = 'Rua Vereador Lindolfo Soares de Carvalho'
      user.password = '123123123'
      user.password_confirmation = '123123123'
    end
    1.times do
      charge = Charge.new
      charge.who_to_charge = Faker::Name.name_with_middle
      charge.address = Faker::Address.full_address
      charge.phone1 = Faker::PhoneNumber.cell_phone
      charge.phone2 = Faker::PhoneNumber.cell_phone
      charge.description = Faker::Lorem.paragraph
      charge.value = 250
      charge.charge_at = Date.today
      charge.paid = false
      charge.save

      charge = Charge.new
      charge.who_to_charge = Faker::Name.name_with_middle
      charge.address = Faker::Address.full_address
      charge.phone1 = Faker::PhoneNumber.cell_phone
      charge.phone2 = Faker::PhoneNumber.cell_phone
      charge.description = Faker::Lorem.paragraph
      charge.value = 250
      charge.charge_at = Date.today
      charge.paid = true
      charge.save
    end
  end

  task insert_data: :environment do
    user = User.where(email: 'rodrigo@rtoledo.inf.br').first_or_create do |user|
      user.name = 'Rodrigo Toledo'
      user.phone = '33991221596'
      user.address = 'Rua Vereador Lindolfo Soares de Carvalho'
      user.password = '123123123'
      user.password_confirmation = '123123123'
    end
    1.times do
      charge = Charge.new
      charge.who_to_charge = Faker::Name.name_with_middle
      charge.address = Faker::Address.full_address
      charge.phone1 = Faker::PhoneNumber.cell_phone
      charge.phone2 = Faker::PhoneNumber.cell_phone
      charge.description = Faker::Lorem.paragraph
      charge.value = 250
      charge.charge_at = Date.today
      charge.paid = false
      charge.save

      charge = Charge.new
      charge.who_to_charge = Faker::Name.name_with_middle
      charge.address = Faker::Address.full_address
      charge.phone1 = Faker::PhoneNumber.cell_phone
      charge.phone2 = Faker::PhoneNumber.cell_phone
      charge.description = Faker::Lorem.paragraph
      charge.value = 250
      charge.charge_at = Date.today
      charge.paid = true
      charge.save
    end
  end

end
