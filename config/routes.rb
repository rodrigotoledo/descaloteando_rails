require 'sidekiq/web'
Rails.application.routes.draw do
  mount ActionCable.server => '/cable'
  mount Sidekiq::Web => '/sidekiq'
  # devise_for :users
  post '/sign_in', to: 'auth#sign_in', as: 'sign_in'
  root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :charges, defaults: {formats: :json}

end
