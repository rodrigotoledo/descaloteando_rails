require 'test_helper'

class ChargesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @new_charge = charges(:charge).dup
    @charge = charges(:charge)
  end

  test "GET status OK" do
    get charges_path(format: :json)
    assert_response :success
  end

  test "GET charges list" do
    get charges_path(format: :json)
    assert_equal json_body['pending_charges'].count, 10
    assert_equal json_body['paid_charges'].count, 21
  end

  test "GET count informations about charges" do
    get charges_path(format: :json)
    assert_equal json_body['two_weeks_ago'], 10
    assert_equal json_body['two_weeks_more'], 21
  end

  test "CREATE charge with status OK" do
    post charges_path(format: :json), params: {charge: @new_charge.attributes}
    assert_response :success
  end

  test "CREATE charge and get informations about it" do
    post charges_path(format: :json), params: {charge: @new_charge.attributes}
    assert_equal json_body['who_to_charge'], @new_charge.who_to_charge
    assert_equal json_body['address'], @new_charge.address
    assert_equal json_body['phone1'], @new_charge.phone1
    assert_equal json_body['phone2'], @new_charge.phone2
    assert_equal json_body['description'], @new_charge.description
    assert_equal json_body['value'], @new_charge.value
    assert_equal json_body['charge_at'], I18n.l(@new_charge.charge_at)
    assert_equal json_body['paid'], @new_charge.paid
  end

  test "FAIL ON CREATE charge and get errors about it" do
    post charges_path(format: :json), params: {charge: {
      who_to_charge: "", address: "", phone1: "", phone2: "",
      description: "", value: "", charge_at: "", paid: ""}}
    assert_equal json_body['errors'].size, 7
  end
end
