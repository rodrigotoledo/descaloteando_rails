ENV['RAILS_ENV'] ||= 'test'
require 'simplecov'
SimpleCov.start do
  add_filter 'test'
end
require_relative '../config/environment'
require 'rails/test_help'
require 'faker'
Dir[Rails.root.join('test', 'support', '**', '*.rb')].each { |f| require f }


class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  include JsonBodyHelper

  # Add more helper methods to be used by all tests here...
end
