class Charge < ApplicationRecord
  scope :pending, -> { where(:paid => false)}
  scope :is_ok, -> { where(:paid => true)}
  scope :two_weeks_ago, -> { where("charge_at >= ?", 2.weeks.ago)}
  scope :two_weeks_more, -> { where("charge_at <= ?", 2.weeks.ago)}
  validates :who_to_charge, :address, :phone1, :phone2, :description, :value, :charge_at, presence: true
end
