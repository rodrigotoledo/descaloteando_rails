json.pending_charges do
  json.array! @charges_informations[:pending_charges] do |charge|
    json.id charge['id']
    json.who_to_charge charge['who_to_charge']
    json.address charge['address']
    json.phone1 charge['phone1']
    json.description charge['description']
    json.value charge['value']
    json.paid charge['paid']
    json.charge_at I18n.l(charge['charge_at'])
    json.created_at I18n.l(charge['created_at'].to_date)
  end
end

json.paid_charges do
  json.array! @charges_informations[:paid_charges] do |charge|
    json.id charge['id']
    json.who_to_charge charge['who_to_charge']
    json.address charge['address']
    json.phone1 charge['phone1']
    json.description charge['description']
    json.value charge['value']
    json.paid charge['paid']
    json.charge_at I18n.l(charge['charge_at'])
    json.created_at I18n.l(charge['created_at'].to_date)
  end
end

json.two_weeks_ago @charges_informations[:two_weeks_ago]
json.two_weeks_more @charges_informations[:two_weeks_more]
