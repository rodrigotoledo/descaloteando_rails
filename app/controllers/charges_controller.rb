class ChargesController < ApplicationController
  before_action :set_charge, only: [:edit, :update, :destroy]

  def index
    @charges_informations = {pending_charges: Charge.pending, paid_charges: Charge.is_ok, two_weeks_ago: Charge.two_weeks_ago.count, two_weeks_more: Charge.two_weeks_more.count}
  end

  def create
    @charge = Charge.new(charge_params)
    unless @charge.save
      render json: {errors: @charge.errors.full_messages}
    end
  end

  def edit
  end

  def update
  end

  private
    def charge_params
      params.require(:charge).permit(:who_to_charge, :address, :phone1, :phone2, :description, :value, :charge_at, :paid)
    end

    def set_charge
      @charge ||= Charge.find(params[:id])
    end
end
