class AuthController < API::APIController
  def sign_in # TODO: How to filter user by its account?
    if [ :email, :password ].all?{ |k| params.key? k }
      user = User.where(email: params[:email]).first
      if user && user.valid_password?(params[:password]) && user.authentication_token?
        begin
          render json: user.attributes, status: :created
        rescue Exception => e
          if [ 'unknown_device', 'unknown_application' ].include?(e.message)
            head :bad_request
          else
            raise e
          end
        end
      else
        head :unauthorized
      end
    else
      head :bad_request
    end
  end
end
