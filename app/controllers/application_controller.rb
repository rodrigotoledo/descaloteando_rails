class ApplicationController < ActionController::API
  # protect_from_forgery with: :null_session

  respond_to :json

  protected
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :phone, :address])
      devise_parameter_sanitizer.permit(:account_update, keys: [:name, :phone, :address])
    end


    def authenticate_user_from_token!
      auth_token = params[:auth_token].presence
      user       = auth_token && User.where(authentication_token: auth_token.to_s).first

      if user
        # Notice we are passing store false, so the user is not
        # actually stored in the session and a token is needed
        # for every request. If you want the token to work as a
        # sign in token, you can simply remove store: false.
        sign_in user, store: false
      else
        head :unauthorized
      end
    end
end
